class Parser:
    def __init__(self, kpl):
        self.kpl = kpl
        self.kpl_pairs = []
        for x in range(len(kpl)-1):
            self.kpl_pairs.append([kpl[x], kpl[x+1]])
        self.averages = [0] * 27
        self.gaps = [0] * 800
        self.combos = list(range(97,123))
        self.combos.append(32)
        self.ml = []
        for x in self.combos:
            for y in self.combos:
                self.ml.append((x,y))

    #ml is the master list of key pairs, kpl_pairs is a set of key pairs
    def getElementsIn(self):
        for x in range(len(self.ml)):
            for y in self.kpl_pairs:
                temp = []
                if y[0].key == self.ml[x][0] and y[1].key == self.ml[x][1]:
                    temp.append(y)
                    self.kpl_pairs.remove(y)
                if len(temp) > 0:
                    self.calculateGap(temp)

    #done before getElementsIn for individual key press times' averages.

    def calculateAvg(self):
        num = 0
        timeT = 0
        for x in self.kpl:
            for y in self.kpl:
                if(y == x):
                    num += 1
                    timeT += x.calculate()
                    self.kpl.remove(y)
            avgTime = timeT/num
            i = self.combos.index(x.key)
            self.averages[i] = avgTime

    #calculates gap between key presses for inputed key object pairs (kpl_pairs)
    def calculateGap(self, temp):
        num = 0
        gapT = 0
        for x in temp:
            num += 1
            gapT += abs(x[0].utime - x[1].dtime)
        avgGap = gapT/num
        i = (27 * self.combos.index(temp[0][0].key)) + (self.combos.index(temp[0][1].key))
        self.gaps[i] = avgGap

    def calculateML(self):
        pass
