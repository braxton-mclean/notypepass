var hasEdited = false;
var autofillOn = false;

$(document).ready(function(){init()});

function init() {
  chrome.storage.sync.get('username', function(result) {
    $("#usernamein").val(result.username);
    setAuthColors();
  });

  //Add checkmark for user authentication
  //Color scheme: yellow/blue: authenticate in Python? Green, good to go?
  $('#usernamein').blur(function() {
    chrome.storage.sync.get('username', function(result) {
      $("#usernamein").val(result.username);
      setAuthColors();
    });
  });


  $('#userform').on('keyup keypress', function(e) {

    var keyCode = e.keyCode || e.which;
    hasEdited = true;
    if(keyCode === 13){
      e.preventDefault();
      chrome.storage.sync.set({'username': $("#usernamein").val()}, function() {
        setAuthColors();
      });
    } else {
      setNormalColors();
    }
  });

  chrome.storage.sync.get('autofill', function(result) {
      if(result.autofill) {
        $('#autotoggle').html('Turn auto-fill off')
        autofillOn = true;
        $('#autotoggle').addClass('btn-success')
                        .removeClass('btn-default')

      } else {
        $('#autotoggle').html('Turn auto-fill on')
        autofillOn = true;
        $('#autotoggle').addClass('btn-default')
                        .removeClass('btn-success')
      }
    });

  $('#autotoggle').on('click', function(e) {

    $('#autotoggle').toggleClass('btn-success');
    if($('#autotoggle').hasClass('btn-success')) {
      $('#autotoggle').removeClass('btn-default');
    } else {
      $('#autotoggle').addClass('btn-default');
    }
    updateAutotoggle();
  });
  $('#optionsbtn').on('click', function(){chrome.runtime.openOptionsPage()});
}

function setNormalColors() {
  $('#usernamein').css('background', '#ffffff')
                  .css('color', '#000000')
                  .css('font-weight', 'normal');
}

function setAuthColors() {
  $('#usernamein').css('background', '#1ec71e')
                  .css('color', '#ffffff')
                  .css('font-weight', 'bold');
}

function updateAutotoggle() {
  if($('#autotoggle').hasClass('btn-success')) {
    $('#autotoggle').html('Turn auto-fill off');
     autofillOn = true;
    chrome.storage.sync.set({'autofill': true}, function() {});
  } else {
   $('#autotoggle').html('Turn auto-fill on');
    autofillOn = false;
   chrome.storage.sync.set({'autofill': false}, function() {});
 }
}