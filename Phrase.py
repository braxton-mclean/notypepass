import random
import os

class PhraseGen:
    def __init__(self):
        pass
    def masterPhrase(self):
        with open('/usr/share/dict/words') as f:
            words = [word.strip().lower() for word in f if ("'" not in word and len(word) > 4 and "é" not in word)]
        password = ' '.join((random.choice(words) for i in range(6)))
        return password

    def senPhrase(self):
        dir = os.path.dirname(__file__)
        password = ""
        filenames = [os.path.join(dir, 'assets/adjectives.txt'),
                     os.path.join(dir, 'assets/nouns.txt'),
                     os.path.join(dir, 'assets/adverbs.txt'),
                     os.path.join(dir, 'assets/verbs.txt'),
                     os.path.join(dir, 'assets/adjectives.txt'),
                     os.path.join(dir, 'assets/nouns.txt')]
        password = []
        for filename in filenames:
            password.append(self.senAppend(filename, password))
        #password[0] = password[0].title()
        password = ''.join(password)
        return password

    def senAppend(self, file, currPass):
        newWord = ""
        if(len(currPass) != 0):
            newWord += " "
        with open(file) as f:
            words = [word.strip().lower() for word in f if ("'" not in word and len(word) > 4) and len(word) < 7 and "é" not in word]
        newWord += (random.choice(words))
        return newWord

if __name__ == "__main__":
    myGen = PhraseGen()
    print(myGen.masterPhrase())
    print(myGen.masterPhrase())
