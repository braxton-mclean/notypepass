# notypepass

NoTypePass is the implementation of keyboard dynamics for user recongition, aiming to replace the need for a password.

Instead of relying on a super-password for a password manager, or managing many other passwords, NoTypePass plans to use the uniqueness of the user's typing style to recognize and authorize their presence. NoTypePass does so by gathering user data of key input times, their depress lengths, along with times between indivual sets of keys being pressed. We use machine learning to generalize these patterns and large data sets to a function, and cross check user input values on time of authorization with the accepted values.

For instance, instead of having a large password of convoluted symbol replacements and numbers strewn about, NoTypePass will generate a random phrase of 6 words, and ask the user to type it out. Upon completion, NoTypePass outputs this data for the server to read, and determines whether or not the present user is the one in question. No passwords necessary.
As a failsafe, NoTypePass has a "MasterPassphrase", a random set of words following no formatting, that will be hashed, and stored on the server for user verification in case the user can no longer type in a similar manner as to their input data (like a new keyboard, injury, prolonged lack of interaction with the keyboard, etc.). It is the user's job to keep hold of this information, as it will only be displayed once. While contrary to the mission of the app, the world can't be assumed perfect.

NoTypePass uses several bundled softwares in anaconda3, along with pygame, numpy, etc. To run the application, navigate to the directory containing the README file, and run "python TypePass.py"