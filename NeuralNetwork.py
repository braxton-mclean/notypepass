from sklearn.neural_network import MLPClassifier
import cloudpickle as pk
import numpy as np

class NeuralNetwork:
    def __init__(self, inputs):
        if inputs == None:
            inputSample = pk.load(open("data/inputData%f.pkl"%int(0), "rb")) #This giant set of data contains an array of dimensions [<input>][10][828]
        else:
            inputSample = inputs
        self.testData(inputSample)

    def getInputData(self, quantity):
        bigData = []
        for x in range(quantity):
            for y in pk.load(open("data/inputData%f.pkl"%int(x), "rb")):
                bigData.append(y)
        return bigData

    def testData(self, inputs):
        print(self.cnnInitMLP(inputs))

    def cnnInitMLP(self, d):
        X_train, X_test = d[0][2:10][:round(len(d) * .8)], d[0][2:10][:round(len(d) * .8)]
        y_train, y_test = d[1], d[1]
        try:
            mlp = open("data/ConvNeuralNet.pkl", "rb")
        except:
            mlp = MLPClassifier(
                hidden_layer_sizes=(500, 1000, 1000, 1000, 500),
                activation="relu",
                solver="lbfgs",
                learning_rate="constant",
                max_iter=1000,
                shuffle=True,
                tol=1e-8,
                learning_rate_init=0.0001,
                verbose=True,
                early_stopping=True,
                validation_fraction=0.1,
                warm_start=True
            )
            mlp.fit(X_train, y_train)
        #print(mlp.score(X_test, y_test))
        #print(mlp.predict(X_test))
        pk.dump(mlp, open("data/ConvNeuralNet.pkl", "wb"))
        return mlp.predict(X_test)

    def cnnRateTest(self):
        rates = np.ndarray(shape=(15, 2), dtype=np.float64)
        for x in range(1, 16):
            rates[x - 1] = [x / 10, self.cnnInit(x / 10)]
        return rates

    def averageAccuraciesByRateValue(self, avgs, trial, generationValue):
        avg = avgs
        if not avg[0][0] == 0.1:
            avg = trial
        else:
            for iter in range(len(avg)):
                avg[iter][1] = ((avg[iter][1] * generationValue) + trial[iter][1]) / (2 + generationValue)
        return avg

app = NeuralNetwork(None)
